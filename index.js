// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");

// Khởi tạo app express
const app = express();

// Import thư viện path
const path = require('path');

// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// Import middleware
const routerMiddleware = require('./middlewares/middleware');

app.use("/",routerMiddleware);

app.use((req, res, next) => {
 let today = new Date();

 console.log("Current: ", today);

 next();
});

app.use((req, res, next) => {
 var fullUrl = req.protocol + '://' + req.get('host') + req.path;
 console.log("Request URL: ", fullUrl);

 next();
});

app.get("/", (req, res) => {
 console.log(__dirname);

 res.sendFile(path.join(__dirname + '/views/SignIn.html'));

});

app.use(express.static(__dirname + "/views"));

// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
 console.log("App listening on port: ", port);
});
